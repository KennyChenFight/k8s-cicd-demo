FROM golang:1.13-alpine
WORKDIR /k8s-cicd-demo
ADD . /k8s-cicd-demo
RUN cd /k8s-cicd-demo && go build
ENTRYPOINT ./k8s-cicd-demo